﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using HIA.Models;

namespace HIA.Controllers
{
    public class QuestionsController : Controller
    {
        private HIADBEntities db = new HIADBEntities();
        private PorterStemmer porterstemmer = new PorterStemmer();
        private RemoveStopWords stopWords = new RemoveStopWords();

        // GET: Questions
        public ActionResult Index()
        {
            var question = db.Question.Include(q => q.Language);
            return View(question.ToList());
        } 
        
        public ActionResult EnglishQuestions()
        {
            var question = db.Question.Include(q => q.Language);
            var englishQuestion = question.Where(q => q.lang_id == 1).ToList();

            return View(englishQuestion);
        }
        public ActionResult ArabicQuestions()
        {
            var question = db.Question.Include(q => q.Language);
            var arabicQuestion = question.Where(q => q.lang_id == 2).ToList();

            return View(arabicQuestion);
        }

        public ActionResult ChooseLanguage(int id)
        {
            if (id == 1)
            {
                ViewBag.language = 1;
            }
            else
            {
                ViewBag.language = 2;
            }

            return View();
        }

        // GET: Questions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Question.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // GET: Questions/Create
        public ActionResult Create()
        {
            ViewBag.lang_id = new SelectList(db.Language, "id", "lang_name");
            return View();
        }

        // POST: Questions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,question1,answer,lang_id")] Question question)
        {
   
            if (ModelState.IsValid)
            {
                int numberOfQuestion                     = db.Question.Count();
                int numberOfQuestionContainsKey          = 0;
                string SearchText                        = question.question1 + " " + question.answer;
                string SearchQuery                       = SearchText.ToLower();
                List<string> questionToList              = testQuestion(SearchQuery);
                Dictionary<string, int>  Indexes         = new Dictionary<string, int>();
        

                    // Add Question to Questions Table
                    db.Question.Add(question);
                    db.SaveChanges();

                    foreach (var item in questionToList)
                    {
                        if (Indexes.ContainsKey(item))
                        {
                            Indexes[item] = Indexes[item] + 1;
                        }
                        else
                        {
                            Indexes.Add(item, 1);
                        }
                    }

                    foreach (var item in Indexes)
                    {
                        if(question.lang_id == 1)
                        {
                            EnglishIndex index = new EnglishIndex()
                            {
                                term = item.Key,
                                question_id = question.id,
                                tf = (double)item.Value / (double)Indexes.Keys.Count()
                            };

                            numberOfQuestionContainsKey = db.EnglishIndex.Where(j => j.term == item.Key && j.question_id != question.id).Distinct().Count() + 1;
                            index.idf = Math.Log10(numberOfQuestion / numberOfQuestionContainsKey);
                            if (index.idf < 0)
                                index.idf = 0;
                            index.weight = index.tf * index.idf;
                            db.EnglishIndex.Add(index);
                            db.SaveChanges();
                        }

                        else
                        {
                            ArabicIndex index = new ArabicIndex()
                            {
                                term        = item.Key,
                                question_id = question.id,
                                tf          = (double)item.Value / (double)Indexes.Keys.Count()
                            };

                            numberOfQuestionContainsKey = db.ArabicIndex.Where(j => j.term == item.Key && j.question_id != question.id).Distinct().Count() + 1;
                            index.idf                       = Math.Log10(numberOfQuestion / numberOfQuestionContainsKey);
                            if (index.idf < 0)
                                index.idf = 0;
                            index.weight                    = index.tf * index.idf;
                            db.ArabicIndex.Add(index);
                            db.SaveChanges();
                        }

                    }

            }

            ViewBag.lang_id = new SelectList(db.Language, "id", "lang_name", question.lang_id);

            return RedirectToAction("Index");
        }

        // GET: Questions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Question.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            ViewBag.lang_id = new SelectList(db.Language, "id", "lang_name", question.lang_id);
            return View(question);
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,question1,answer,lang_id")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.lang_id = new SelectList(db.Language, "id", "lang_name", question.lang_id);
            return View(question);
        }

        // GET: Questions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Question.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Question.Find(id);
            db.Question.Remove(question);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<string> testQuestion(string text)
        {
            bool detectLanguage = stopWords.HasArabicCharacters(text);

            List<string> listOfWord;

            if (detectLanguage == true)
            {
                // Arabic stop word -- return text without stop words
                string removeArabicStopWord = stopWords.StopWordsStrip(text);

                // Arabic stemmer --- return all words to root
                string arabicStemming = stopWords.ArabicStemming(removeArabicStopWord);

                // make all words as list without empty.
                listOfWord = porterstemmer.stringToList(arabicStemming);

            }

            else
            {
                // English stop word -- return text without stop words
                string removeEnglishStopWord = porterstemmer.englishStopWordsStrip(text);

                // Englisgh stemmer --- return all words to root
                string englishStemmerText = porterstemmer.englishWordsStrip(removeEnglishStopWord);

                // make all words as list without empty.
                listOfWord = porterstemmer.stringToList(englishStemmerText);
                
            }


            return listOfWord;

        }

    }
}

