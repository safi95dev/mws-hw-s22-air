﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HIA.Models;
using System.Linq.Dynamic;
using System;
using System.Diagnostics;

namespace HIA.Controllers
{
    public class HomeController : Controller
    {
        private HIADBEntities _db;
        private PorterStemmer porterstemmer;
        private RemoveStopWords stopWords;

        public HomeController()
        {
            _db = new HIADBEntities();
            porterstemmer = new PorterStemmer();
            stopWords = new RemoveStopWords();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult BooleanModel(string searchText)
        {

            string SearchQuery = searchText.ToLower();

            List<Question> Docs = new List<Question>();
            List<Question> DocsAND = new List<Question>();
            List<Question> DocsEB = new List<Question>();
            List<Question> questionList = new List<Question>();


            bool detectLanguage = stopWords.HasArabicCharacters(searchText);

            if (detectLanguage == true)
            {
                //Arabic string without stop words
                string removeArabicStopWord = stopWords.StopWordsStrip(SearchQuery);

                // Arabic stemmer
                string arabicStemming = stopWords.ArabicStemming(removeArabicStopWord);

                ViewBag.resultLabel = "عرض نتايج البحث الموافقة لـ";

                List<string> listOfWord = porterstemmer.stringToList(arabicStemming);

                var arabicIndexes = from arabic in _db.ArabicIndex
                                    select arabic;

                List<int> ID = new List<int>();

                var ArQuery = "";

                for (int i = 0; i < listOfWord.Count(); i++)
                {
                    var item = listOfWord.ElementAt(i);

                    if (i == 0)
                    {
                        DocsAND = _db.ArabicIndex.Where(ind => ind.term == item).Select(ind => ind.Question).Distinct().ToList();
                    }

                    else
                    {
                        var temp = _db.ArabicIndex.Where(ind => ind.term == item).Select(ind => ind.Question).Distinct().ToList();

                        DocsAND = DocsAND.Intersect(temp).ToList();
                    }

                    if (i != 0 && i != listOfWord.Count())
                    {
                        ArQuery += " OR ";
                    }

                    ArQuery += " term = \"" + item + "\" ";

                }

                arabicIndexes = arabicIndexes.Where(ArQuery);

                if (arabicIndexes.ToList() != null && arabicIndexes.Count() > 0)
                {
                    foreach (var item in arabicIndexes.Select(d => d.Question).Distinct())
                    {
                        if (!ID.Contains(item.id))
                        {
                            ID.Add(item.id);
                            Docs.Add(item);
                        }
                    }
                }

                ViewBag.Result = Docs;
                ViewBag.ResultAND = DocsAND;

                foreach (var question in Docs)
                {
                    questionList.Add(question);
                }
            }

            else
            {

                //English string without stop words
                string removeEnglishStopWord = porterstemmer.englishStopWordsStrip(SearchQuery);

                //English stremmer
                string englishStemmerText = porterstemmer.englishWordsStrip(removeEnglishStopWord);

                ViewBag.resultLabel = "Your Result of:";

                List<string> listOfWord = porterstemmer.stringToList(englishStemmerText);

                var Englishndexes = from EnglishIndex in _db.EnglishIndex
                                    select EnglishIndex;


                List<int> ID = new List<int>();

                var EnQuery = "";

                for (int i = 0; i < listOfWord.Count(); i++)
                {
                    var item = listOfWord.ElementAt(i);

                    if (i == 0)
                    {
                        DocsAND = _db.EnglishIndex.Where(ind => ind.term == item).Select(ind => ind.Question).Distinct().ToList();
                    }

                    else
                    {
                        var temp = _db.EnglishIndex.Where(ind => ind.term == item).Select(ind => ind.Question).Distinct().ToList();

                        DocsAND = DocsAND.Intersect(temp).ToList();
                    }

                    if (i != 0 && i != listOfWord.Count())
                    {
                        EnQuery += " OR ";
                    }
                    EnQuery += " term = \"" + item + "\" ";
                }

                Englishndexes = Englishndexes.Where(EnQuery);

                if (Englishndexes.ToList() != null && Englishndexes.Count() > 0)
                {
                    foreach (var item in Englishndexes.Select(d => d.Question).Distinct())
                    {
                        if (!ID.Contains(item.id))
                        {
                            ID.Add(item.id);
                            Docs.Add(item);
                        }
                    }
                }

                ViewBag.Result = Docs;
                ViewBag.ResultAND = DocsAND;
               

                foreach (var question in Docs)
                {
                    questionList.Add(question);
                }

            }


            ViewData["TXT"] = searchText;

            return View(questionList);

        }

        public ActionResult ExtendedBooleanModel(string searchText)
        {
            string SearchQuery = searchText.ToLower();

            List<Question> Docs     = new List<Question>();
            List<Question> DocsAND  = new List<Question>();
            List<Question> questionList = new List<Question>();


            bool detectLanguage = stopWords.HasArabicCharacters(searchText);

            if (detectLanguage == true)
            {
                //Arabic string without stop words
                string removeArabicStopWord = stopWords.StopWordsStrip(SearchQuery);

                ViewBag.resultLabel = "عرض نتايج البحث الموافقة لـ";

                // Arabic stemmer
                string arabicStemming = stopWords.ArabicStemming(removeArabicStopWord);

                List<string> listOfWord = porterstemmer.stringToList(arabicStemming);


                var SearchAr = from d in _db.ArabicIndex
                               select d;

                List<int> ID = new List<int>();

                string ArQuery = "";

                for (int i = 0; i < listOfWord.Count(); i++)
                {
                    var item = listOfWord.ElementAt(i);
                    if (i != 0 && i != listOfWord.Count())
                    {
                        ArQuery += " OR ";
                    }
                    ArQuery += " Term = \"" + item + "\" ";
                }

                SearchAr = SearchAr.Where(ArQuery).Distinct();

                if (SearchAr.ToList() != null && SearchAr.Count() > 0)
                {
                    ID.AddRange(SearchAr.Select(d => d.Question).Select(d => d.id).Distinct());
                    Docs.AddRange(SearchAr.Select(d => d.Question).Distinct());

                    foreach (var d in Docs)
                    {
                        double wd = 0;
                        foreach (var i in SearchAr)
                        {
                            wd += (double)(i.weight * i.weight);
                        }

                        d.Sim = Math.Sqrt((double)wd / (double)SearchAr.Count());

                        questionList.Add(d);
                    }
                }


            }

            else
            {
                //English string without stop words
                string removeEnglishStopWord = porterstemmer.englishStopWordsStrip(SearchQuery);

                //English stremmer
                string englishStemmerText = porterstemmer.englishWordsStrip(removeEnglishStopWord);

                ViewBag.resultLabel = "Your Result of:";

                List<string> listOfWord = porterstemmer.stringToList(englishStemmerText);

                var SearchEn = from d in _db.EnglishIndex
                               select d;


                List<int> ID = new List<int>();

                string EnQuery = "";

                for (int i = 0; i < listOfWord.Count(); i++)
                {
                    var item = listOfWord.ElementAt(i);
                    if (i != 0 && i != listOfWord.Count())
                    {
                        EnQuery += " OR ";
                    }
                    EnQuery += " Term = \"" + item + "\" ";
                }

                SearchEn = SearchEn.Where(EnQuery).Distinct();

                if (SearchEn.ToList() != null && SearchEn.Count() > 0)
                {
                    var NewDocuments = new List<Question>();
                    var EnIndecies = SearchEn;
                    foreach (var item in SearchEn.Select(d => d.Question).Distinct())
                    {

                        if (!ID.Contains(item.id))
                        {
                            ID.Add(item.id);
                            Docs.Add(item);
                            NewDocuments.Add(item);
                        }
                    }

                    foreach (var item in NewDocuments)
                    {
                        double wd = 0;
                        foreach (var i in EnIndecies)
                        {
                            wd += (double)(i.weight * i.weight);
                        }
                        item.Sim = Math.Sqrt((double)wd / (double)EnIndecies.Count());
                        questionList.Add(item);
                    }

                }
            }

            ViewBag.EB = "selected";
            ViewBag.Result = questionList.OrderBy(d => d.Sim).ToList();
            ViewData["TXT"] = searchText;

            return View(questionList);

        }

        public ActionResult VectorModel(string searchText)
        {

            string SearchQuery = searchText.ToLower();

            List<Question> Docs = new List<Question>();
            List<Question> DocsAND = new List<Question>();
            List<Question> questionList = new List<Question>();


            bool detectLanguage = stopWords.HasArabicCharacters(searchText);

            if (detectLanguage == true)
            {
                //Arabic string without stop words
                string removeArabicStopWord = stopWords.StopWordsStrip(SearchQuery);

                ViewBag.resultLabel = "عرض نتايج البحث الموافقة لـ";

                // Arabic stemmer
                string arabicStemming = stopWords.ArabicStemming(removeArabicStopWord);

                List<string> listOfWord = porterstemmer.stringToList(arabicStemming);


                var SearchAr = from d in _db.ArabicIndex
                               select d;

                List<int> ID = new List<int>();

                string ArQuery = "";
                for (int i = 0; i < listOfWord.Count(); i++)
                {
                    var item = listOfWord.ElementAt(i);
                    if (i != 0 && i != listOfWord.Count())
                    {
                        ArQuery += " OR ";
                    }
                    ArQuery += " Term = \"" + item + "\" ";
                }
                SearchAr = SearchAr.Where(ArQuery).Distinct();

                if (SearchAr.ToList() != null && SearchAr.Count() > 0)
                {
                    ID.AddRange(SearchAr.Select(d => d.Question).Select(d => d.id).Distinct());
                    Docs.AddRange(SearchAr.Select(d => d.Question).Distinct());
                    foreach (var d in Docs)
                    {
                        double Q = 0;
                        foreach (var i in SearchAr)
                        {
                            Q += (double)(i.weight * i.weight);
                        }
                        double NorQ = Math.Sqrt(Q);

                        double Di = 0;
                        foreach (var i in _db.ArabicIndex.Where(x => x.question_id == d.id).ToList())
                        {
                            Di += (double)(i.weight * i.weight);
                        }
                        double NorDi = Math.Sqrt(Di);

                        d.Sim = (Q * Di) / (double)(NorQ * NorDi);

                        questionList.Add(d);
                    }
                }
            }

            else
            {

                //English string without stop words
                string removeEnglishStopWord = porterstemmer.englishStopWordsStrip(SearchQuery);

                ViewBag.resultLabel = "Your Result of:";
                //English stremmer
                string englishStemmerText = porterstemmer.englishWordsStrip(removeEnglishStopWord);

                List<string> listOfWord = porterstemmer.stringToList(englishStemmerText);


                var SearchEn = from d in _db.EnglishIndex
                               select d;


                List<int> ID = new List<int>();
                string EnQuery = "";
                for (int i = 0; i < listOfWord.Count(); i++)
                {
                    var item = listOfWord.ElementAt(i);
                    if (i != 0 && i != listOfWord.Count())
                    {
                        EnQuery += " OR ";
                    }
                    EnQuery += " Term = \"" + item + "\" ";
                }

                SearchEn = SearchEn.Where(EnQuery).Distinct();

                if (SearchEn.ToList() != null && SearchEn.Count() > 0)
                {
                    var newDocuments = new List<Question>();
                    var EnIndecies = SearchEn;
                    foreach (var item in SearchEn.Select(d => d.Question).Distinct())
                    {

                        if (!ID.Contains(item.id))
                        {
                            ID.Add(item.id);
                            Docs.Add(item);
                            newDocuments.Add(item);
                        }
                    }
                    foreach (var item in newDocuments)
                    {
                        double Q = 0;
                        foreach (var i in EnIndecies)
                        {
                            Q += (double)(i.weight * i.weight);
                        }
                        double NorQ = Math.Sqrt(Q);

                        double Di = 0;
                        foreach (var i in _db.EnglishIndex.Where(x => x.question_id == item.id).ToList())
                        {
                            Di += (double)(i.weight * i.weight);
                        }
                        double NorDi = Math.Sqrt(Di);

                        item.Sim = (Q * Di) / (double)(NorQ * NorDi);
                        questionList.Add(item);
                    }
                }
            }

            ViewData["TXT"] = searchText;

            return View(questionList);
        }

    }
}