﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace HIA.Models
{
    public class PorterStemmer
    {

        private char[] b;
        private int i,     /* offset into b */
            i_end, /* offset to end of stemmed word */
            j, k;
        private static int INC = 200;
        /* unit of size whereby b is increased */

        public PorterStemmer()
        {
            b = new char[INC];
            i = 0;
            i_end = 0;
        }

        //Alter by IA 2020-11-04 
        public string englishWordsStrip(string text)
        {
            string Stem = "";

            string S = "";

            for (int l = 0; l < text.Length; l++)
                if (char.IsLetter(text, l))
                    S += text[l].ToString();
                else
                    S += " ";
            S = S.Trim();

            string[] R = S.Split(' ');

            for (int j = 0; j < R.Length; j++)
            {
                Stem += StemWord(R[j]);
                Stem += " ";
            }


            return Stem;
        }


        /* Implementation of the .NET interface - added as part of realease 4 (Leif) */
        public string StemWord(string s)
        {
            setTerm(s);
            stem();
            return getTerm();
        }

        /*
            SetTerm and GetTerm have been simply added to ease the 
            interface with other lanaguages. They replace the add functions 
            and toString function. This was done because the original functions stored
            all stemmed words (and each time a new woprd was added, the buffer would be
            re-copied each time, making it quite slow). Now, The class interface 
            that is provided simply accepts a term and returns its stem, 
            instead of storing all stemmed words.
            (Leif)
        */
        void setTerm(string s)
        {
            i = s.Length;
            char[] new_b = new char[i];
            for (int c = 0; c < i; c++)
                new_b[c] = s[c];

            b = new_b;

        }

        public string getTerm()
        {
            return new String(b, 0, i_end);
        }

        /* Old interface to the class - left for posterity. However, it is not
            * used when accessing the class via .NET (Leif)*/

        /*
            * Add a character to the word being stemmed.  When you are finished
            * adding characters, you can call stem(void) to stem the word.
            */
        public void add(char ch)
        {
            if (i == b.Length)
            {
                char[] new_b = new char[i + INC];
                for (int c = 0; c < i; c++)
                    new_b[c] = b[c];
                b = new_b;
            }
            b[i++] = ch;
        }


        /* Adds wLen characters to the word being stemmed contained in a portion
            * of a char[] array. This is like repeated calls of add(char ch), but
            * faster.
            */
        public void add(char[] w, int wLen)
        {
            if (i + wLen >= b.Length)
            {
                char[] new_b = new char[i + wLen + INC];
                for (int c = 0; c < i; c++)
                    new_b[c] = b[c];
                b = new_b;
            }
            for (int c = 0; c < wLen; c++)
                b[i++] = w[c];
        }

        /*
            * After a word has been stemmed, it can be retrieved by toString(),
            * or a reference to the internal buffer can be retrieved by getResultBuffer
            * and getResultLength (which is generally more efficient.)
            */
        public override string ToString()
        {
            return new String(b, 0, i_end);
        }

        /*
            * Returns the length of the word resulting from the stemming process.
            */
        public int getResultLength()
        {
            return i_end;
        }

        /*
            * Returns a reference to a character buffer containing the results of
            * the stemming process.  You also need to consult getResultLength()
            * to determine the length of the result.
            */
        public char[] getResultBuffer()
        {
            return b;
        }

        /* cons(i) is true <=> b[i] is a consonant. */
        private bool cons(int i)
        {
            switch (b[i])
            {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u': return false;
                case 'y': return (i == 0) ? true : !cons(i - 1);
                default: return true;
            }
        }

        /* m() measures the number of consonant sequences between 0 and j. if c is
            a consonant sequence and v a vowel sequence, and <..> indicates arbitrary
            presence,

                <c><v>       gives 0
                <c>vc<v>     gives 1
                <c>vcvc<v>   gives 2
                <c>vcvcvc<v> gives 3
                ....
        */
        private int m()
        {
            int n = 0;
            int i = 0;
            while (true)
            {
                if (i > j) return n;
                if (!cons(i)) break; i++;
            }
            i++;
            while (true)
            {
                while (true)
                {
                    if (i > j) return n;
                    if (cons(i)) break;
                    i++;
                }
                i++;
                n++;
                while (true)
                {
                    if (i > j) return n;
                    if (!cons(i)) break;
                    i++;
                }
                i++;
            }
        }

        /* vowelinstem() is true <=> 0,...j contains a vowel */
        private bool vowelinstem()
        {
            int i;
            for (i = 0; i <= j; i++)
                if (!cons(i))
                    return true;
            return false;
        }

        /* doublec(j) is true <=> j,(j-1) contain a double consonant. */
        private bool doublec(int j)
        {
            if (j < 1)
                return false;
            if (b[j] != b[j - 1])
                return false;
            return cons(j);
        }

        /* cvc(i) is true <=> i-2,i-1,i has the form consonant - vowel - consonant
            and also if the second c is not w,x or y. this is used when trying to
            restore an e at the end of a short word. e.g.

                cav(e), lov(e), hop(e), crim(e), but
                snow, box, tray.

        */
        private bool cvc(int i)
        {
            if (i < 2 || !cons(i) || cons(i - 1) || !cons(i - 2))
                return false;
            int ch = b[i];
            if (ch == 'w' || ch == 'x' || ch == 'y')
                return false;
            return true;
        }

        private bool ends(String s)
        {
            int l = s.Length;
            int o = k - l + 1;
            if (o < 0)
                return false;
            char[] sc = s.ToCharArray();
            for (int i = 0; i < l; i++)
                if (b[o + i] != sc[i])
                    return false;
            j = k - l;
            return true;
        }

        /* setto(s) sets (j+1),...k to the characters in the string s, readjusting
            k. */
        private void setto(String s)
        {
            int l = s.Length;
            int o = j + 1;
            char[] sc = s.ToCharArray();
            for (int i = 0; i < l; i++)
                b[o + i] = sc[i];
            k = j + l;
        }

        /* r(s) is used further down. */
        private void r(String s)
        {
            if (m() > 0)
                setto(s);
        }

        /* step1() gets rid of plurals and -ed or -ing. e.g.
                caresses  ->  caress
                ponies    ->  poni
                ties      ->  ti
                caress    ->  caress
                cats      ->  cat

                feed      ->  feed
                agreed    ->  agree
                disabled  ->  disable

                matting   ->  mat
                mating    ->  mate
                meeting   ->  meet
                milling   ->  mill
                messing   ->  mess

                meetings  ->  meet

        */

        private void step1()
        {
            if (b[k] == 's')
            {
                if (ends("sses"))
                    k -= 2;
                else if (ends("ies"))
                    setto("i");
                else if (b[k - 1] != 's')
                    k--;
            }
            if (ends("eed"))
            {
                if (m() > 0)
                    k--;
            }
            else if ((ends("ed") || ends("ing")) && vowelinstem())
            {
                k = j;
                if (ends("at"))
                    setto("ate");
                else if (ends("bl"))
                    setto("ble");
                else if (ends("iz"))
                    setto("ize");
                else if (doublec(k))
                {
                    k--;
                    int ch = b[k];
                    if (ch == 'l' || ch == 's' || ch == 'z')
                        k++;
                }
                else if (m() == 1 && cvc(k)) setto("e");
            }
        }

        /* step2() turns terminal y to i when there is another vowel in the stem. */
        private void step2()
        {
            if (ends("y") && vowelinstem())
                b[k] = 'i';
        }

        /* step3() maps double suffices to single ones. so -ization ( = -ize plus
            -ation) maps to -ize etc. note that the string before the suffix must give
            m() > 0. */
        private void step3()
        {
            if (k == 0)
                return;

            /* For Bug 1 */
            switch (b[k - 1])
            {
                case 'a':
                    if (ends("ational")) { r("ate"); break; }
                    if (ends("tional")) { r("tion"); break; }
                    break;
                case 'c':
                    if (ends("enci")) { r("ence"); break; }
                    if (ends("anci")) { r("ance"); break; }
                    break;
                case 'e':
                    if (ends("izer")) { r("ize"); break; }
                    break;
                case 'l':
                    if (ends("bli")) { r("ble"); break; }
                    if (ends("alli")) { r("al"); break; }
                    if (ends("entli")) { r("ent"); break; }
                    if (ends("eli")) { r("e"); break; }
                    if (ends("ousli")) { r("ous"); break; }
                    break;
                case 'o':
                    if (ends("ization")) { r("ize"); break; }
                    if (ends("ation")) { r("ate"); break; }
                    if (ends("ator")) { r("ate"); break; }
                    break;
                case 's':
                    if (ends("alism")) { r("al"); break; }
                    if (ends("iveness")) { r("ive"); break; }
                    if (ends("fulness")) { r("ful"); break; }
                    if (ends("ousness")) { r("ous"); break; }
                    break;
                case 't':
                    if (ends("aliti")) { r("al"); break; }
                    if (ends("iviti")) { r("ive"); break; }
                    if (ends("biliti")) { r("ble"); break; }
                    break;
                case 'g':
                    if (ends("logi")) { r("log"); break; }
                    break;
                default:
                    break;
            }
        }

        /* step4() deals with -ic-, -full, -ness etc. similar strategy to step3. */
        private void step4()
        {
            switch (b[k])
            {
                case 'e':
                    if (ends("icate")) { r("ic"); break; }
                    if (ends("ative")) { r(""); break; }
                    if (ends("alize")) { r("al"); break; }
                    break;
                case 'i':
                    if (ends("iciti")) { r("ic"); break; }
                    break;
                case 'l':
                    if (ends("ical")) { r("ic"); break; }
                    if (ends("ful")) { r(""); break; }
                    break;
                case 's':
                    if (ends("ness")) { r(""); break; }
                    break;
            }
        }

        /* step5() takes off -ant, -ence etc., in context <c>vcvc<v>. */
        private void step5()
        {
            if (k == 0)
                return;

            /* for Bug 1 */
            switch (b[k - 1])
            {
                case 'a':
                    if (ends("al")) break; return;
                case 'c':
                    if (ends("ance")) break;
                    if (ends("ence")) break; return;
                case 'e':
                    if (ends("er")) break; return;
                case 'i':
                    if (ends("ic")) break; return;
                case 'l':
                    if (ends("able")) break;
                    if (ends("ible")) break; return;
                case 'n':
                    if (ends("ant")) break;
                    if (ends("ement")) break;
                    if (ends("ment")) break;
                    /* element etc. not stripped before the m */
                    if (ends("ent")) break; return;
                case 'o':
                    if (ends("ion") && j >= 0 && (b[j] == 's' || b[j] == 't')) break;
                    /* j >= 0 fixes Bug 2 */
                    if (ends("ou")) break; return;
                /* takes care of -ous */
                case 's':
                    if (ends("ism")) break; return;
                case 't':
                    if (ends("ate")) break;
                    if (ends("iti")) break; return;
                case 'u':
                    if (ends("ous")) break; return;
                case 'v':
                    if (ends("ive")) break; return;
                case 'z':
                    if (ends("ize")) break; return;
                default:
                    return;
            }
            if (m() > 1)
                k = j;
        }

        /* step6() removes a final -e if m() > 1. */
        private void step6()
        {
            j = k;

            if (b[k] == 'e')
            {
                int a = m();
                if (a > 1 || a == 1 && !cvc(k - 1))
                    k--;
            }
            if (b[k] == 'l' && doublec(k) && m() > 1)
                k--;
        }

        /* Stem the word placed into the Stemmer buffer through calls to add().
            * Returns true if the stemming process resulted in a word different
            * from the input.  You can retrieve the result with
            * getResultLength()/getResultBuffer() or toString().
            */
        public void stem()
        {
            k = i - 1;
            if (k > 1)
            {
                step1();
                step2();
                step3();
                step4();
                step5();
                step6();
            }
            i_end = k + 1;
            i = 0;
        }



        public static string[] englishStopWordsList = new string[]
        {
            "'ll","'tis","'twas","'ve","10","39","a","a's","able","ableabout","about","above","abroad","abst",
            "accordance","according","accordingly","across","act","actually","ad","added","adj","adopted","ae","af",
            "affected","affecting","affects","after","afterwards","ag","again","against","ago","ah","ahead","ai",
            "ain't","aint","al","all","allow","allows","almost","alone","along","alongside","already","also",
            "although","always","am","amid","amidst","among","amongst","amoungst","amount","an","and","announce",
            "another","any","anybody","anyhow","anymore","anyone","anything","anyway","anyways","anywhere","ao",
            "apart","apparently","appear","appreciate","appropriate","approximately","aq","ar","are","area",
            "areas","aren","aren't","arent","arise","around","arpa","as","aside","ask","asked","asking","asks",
            "associated","at","au","auth","available","aw","away","awfully","az","b","ba","back","backed",
            "backing","backs","backward","backwards","bb","bd","be","became","because","become","becomes",
            "becoming","been","before","beforehand","began","begin","beginning","beginnings","begins","behind",
            "being","beings","believe","below","beside","besides","best","better","between","beyond","bf",
            "bg","bh","bi","big","bill","billion","biol","bj","bm","bn","bo","both","bottom","br","brief",
            "briefly","bs","bt","but","buy","bv","bw","by","bz","c","c'mon","c's","ca","call","came","can",
            "can't","cannot","cant","caption","case","cases","cause","causes","cc","cd","certain","certainly",
            "cf","cg","ch","changes","ci","ck","cl","clear","clearly","click","cm","cmon","cn","co","co.",
            "com","come","comes","computer","con","concerning","consequently","consider","considering",
            "contain","containing","contains","copy","corresponding","could","could've","couldn","couldn't",
            "couldnt","course","cr","cry","cs","cu","currently","cv","cx","cy","cz","d","dare","daren't",
            "darent","date","de","dear","definitely","describe","described","despite","detail","did","didn",
            "didn't","didnt","differ","different","differently","directly","dj","dk","dm","do","does","doesn",
            "doesn't","doesnt","doing","don","don't","done","dont","doubtful","down","downed","downing","downs",
            "downwards","due","during","dz","e","each","early","ec","ed","edu","ee","effect","eg","eh","eight",
            "eighty","either","eleven","else","elsewhere","empty","end","ended","ending","ends","enough","entirely",
            "er","es","especially","et","et-al","etc","even","evenly","ever","evermore","every","everybody","everyone",
            "everything","everywhere","ex","exactly","example","except","f","face","faces","fact","facts","fairly",
            "far","farther","felt","few","fewer","ff","fi","fifteen","fifth","fifty","fify","fill","find","finds",
            "fire","first","five","fix","fj","fk","fm","fo","followed","following","follows","for","forever",
            "former","formerly","forth","forty","forward","found","four","fr","free","from","front","full","fully",
            "further","furthered","furthering","furthermore","furthers","fx","g","ga","gave","gb","gd","ge","general",
            "generally","get","gets","getting","gf","gg","gh","gi","give","given","gives","giving","gl","gm","gmt",
            "gn","go","goes","going","gone","good","goods","got","gotten","gov","gp","gq","gr","great","greater",
            "greatest","greetings","group","grouped","grouping","groups","gs","gt","gu","gw","gy","h","had","hadn't",
            "hadnt","half","happens","hardly","has","hasn","hasn't","hasnt","have","haven","haven't","havent","having",
            "he","he'd","he'll","he's","hed","hell","hello","help","hence","her","here","here's","hereafter","hereby",
            "herein","heres","hereupon","hers","herself","herse”","hes","hi","hid","high","higher","highest","him",
            "himself","himse”","his","hither","hk","hm","hn","home","homepage","hopefully","how","how'd","how'll",
            "how's","howbeit","however","hr","ht","htm","html","http","hu","hundred","i","i'd","i'll","i'm","i've",
            "i.e.","id","ie","if","ignored","ii","il","ill","im","immediate","immediately","importance","important",
            "in","inasmuch","inc","inc.","indeed","index","indicate","indicated","indicates","information","inner",
            "inside","insofar","instead","int","interest","interested","interesting","interests","into","invention",
            "inward","io","iq","ir","is","isn","isn't","isnt","it","it'd","it'll","it's","itd","itll","its","itself",
            "itse”","ive","j","je","jm","jo","join","jp","just","k","ke","keep","keeps","kept","keys","kg","kh","ki",
            "kind","km","kn","knew","know","known","knows","kp","kr","kw","ky","kz","l","la","large","largely","last",
            "lately","later","latest","latter","latterly",
            "lb","lc","least","length","less","lest","let","let's","lets","li","like","liked","likely","likewise","line",
            "little","lk","ll","long","longer","longest","look","looking","looks","low","lower","lr","ls","lt","ltd","lu",
            "lv","ly","m","ma","made","mainly","make","makes","making","man","many","may","maybe","mayn't","maynt","mc",
            "md","me","mean","means","meantime",
            "meanwhile","member","members","men","merely","mg","mh","microsoft","might","might've","mightn't","mightnt","mil",
            "mill","million","mine","minus","miss","mk","ml","mm","mn","mo","more","moreover","most","mostly","move","mp","mq",
            "mr","mrs","ms","msie","mt","mu","much","mug","must","must've","mustn't","mustnt","mv","mw","mx","my","myself","myse”",
            "mz","n","na","name",
            "namely","nay","nc","nd","ne","near","nearly","necessarily","necessary","need","needed","needing","needn't","neednt",
            "needs","neither","net","netscape","never","neverf","neverless","nevertheless","new","newer","newest","next","nf","ng","ni",
            "nine","ninety","nl","no","no-one","nobody","non","none","nonetheless","noone","nor","normally","nos","not","noted","nothing",
            "notwithstanding","novel","now","nowhere","np","nr","nu","null","number","numbers","nz","o","obtain","obtained","obviously","of",
            "off","often","oh","ok","okay","old","older","oldest","om","omitted","on","once","one","one's","ones","only",
            "onto","open","opened","opening","opens","opposite","or","ord","order","ordered","ordering","orders","org","other","others",
            "otherwise","ought","oughtn't","oughtnt","our","ours","ourselves","out","outside","over","overall","owing","own","p","pa","page",
            "pages","part","parted","particular","particularly","parting","parts","past","pe","per","perhaps","pf","pg","ph","pk",
            "pl","place","placed","places","please","plus","pm","pmid","pn","point","pointed","pointing","points","poorly","possible",
            "possibly","potentially","pp","pr","predominantly","present","presented","presenting","presents","presumably","previously",
            "primarily",
            "probably","problem","problems","promptly","proud","provided","provides","pt","put","puts","pw","py","q","qa","que",
            "quickly","quite","qv","r","ran","rather","rd","re","readily","really","reasonably","recent","recently","ref","refs","regarding",
            "regardless","regards","related","relatively","research","reserved","respectively","resulted","resulting","results","right","ring",
            "ro","room","rooms","round","ru","run","rw","s","sa","said","same","saw","say","saying","says","sb","sc","sd","se",
            "sec","second","secondly","seconds","section","see","seeing","seem","seemed","seeming","seems","seen","sees","self",
            "selves","sensible","sent","serious","seriously","seven","seventy","several","sg","sh","shall","shan't","shant","she",
            "she'd","she'll","she's","shed","shell","shes","should","should've","shouldn","shouldn't","shouldnt",
            "show","showed","showing","shown","showns","shows","si","side","sides","significant","significantly",
            "similar","similarly","since","sincere","site","six","sixty","sj","sk","sl","slightly","sm","small",
            "smaller","smallest","sn","so","some","somebody","someday","somehow","someone","somethan","something",
            "sometime","sometimes","somewhat","somewhere","soon","sorry","specifically","specified","specify","specifying","sr","st",
            "state","states","still","stop","strongly","su","sub","substantially","successfully","such","sufficiently","suggest","sup",
            "sure","sv","sy",
            "system","sz","t","t's","take","taken","taking","tc","td","tell","ten","tends","test","text","tf","tg","th","than",
            "thank","thanks","thanx","that","that'll","that's","that've","thatll","thats","thatve","the","their","theirs","them",
            "themselves","then","thence","there","there'd","there'll","there're","there's","there've","thereafter","thereby","thered",
            "therefore","therein",
            "therell","thereof","therere","theres","thereto","thereupon","thereve","these","they","they'd","they'll","they're",
            "they've","theyd","theyll","theyre","theyve","thick","thin","thing","things","think","thinks","third","thirty","this",
            "thorough","thoroughly","those","thou","though","thoughh","thought","thoughts","thousand","three","throug","through",
            "throughout","thru","thus",
            "til","till","tip","tis","tj","tk","tm","tn","to","today","together","too","took","top","toward","towards","tp",
            "tr","tried","tries","trillion","truly","try","trying","ts","tt","turn","turned","turning","turns","tv","tw",
            "twas","twelve","twenty","twice","two","tz","u","ua","ug","uk","um","un","under","underneath","undoing","unfortunately",
            "unless","unlike","unlikely","until","unto","up","upon","ups","upwards","us","use","used","useful","usefully","usefulness",
            "uses","using",
            "usually","uucp","uy","uz","v","va","value","various","vc","ve","versus","very","vg","vi","via","viz","vn","vol","vols",
            "vs","vu","w","want","wanted","wanting","wants","was","wasn","wasn't","wasnt","way","ways","we","we'd","we'll","we're",
            "we've","web","webpage","website","wed","welcome","well","wells","went","were","weren","weren't","werent","weve","wf",
            "what","what'd","what'll",
            "what's","what've","whatever","whatll","whats","whatve","when","when'd","when'll","when's","whence","whenever","where",
            "where'd","where'll","where's","whereafter","whereas","whereby","wherein","wheres","whereupon","wherever","whether","which",
            "whichever","while","whilst","whim","whither","who","who'd","who'll","who's","whod","whoever","whole","wholl","whom",
            "whomever","whos","whose",
            "why","why'd","why'll","why's","widely","width","will","willing","wish","with","within","without","won","won't","wonder",
            "wont","words","work","worked","working","works","world","would","would've","wouldn",
            "wouldn't","wouldnt","ws","www","x","y","ye","year","years","yes","yet","you","you'd","you'll","you're","you've","youd",
            "youll","young","younger","youngest","your","youre","yours","yourself","yourselves","youve","yt","yu","z","za","zero","zm","zr"
        };

        public string englishRemovingStopWord(string W)
        {
            string Stem = "";
            W = W.Trim();

            string S = W.ToString();
            if (!englishStopWordsList.Contains(S))
            {
                Stem += S;
            }
            return Stem;
        }

        public string englishStopWordsStrip(string text)
        {
            string Stem = "";

            string S = "";

            for (int l = 0; l < text.Length; l++)
                if (char.IsLetter(text, l))
                    S += text[l].ToString();
                else
                    S += " ";
            S = S.Trim();

            string[] R = S.Split(' ');

            for (int j = 0; j < R.Length; j++)
            {
                Stem += englishRemovingStopWord(R[j]);
                Stem += " ";
            }

            return Stem;
        }

        public List<string> stringToList(string text)
        {

            List<string> result = new List<string>();

            string S = "";

            for (int l = 0; l < text.Length; l++)
                if (char.IsLetter(text, l))
                    S += text[l].ToString();
                else
                    S += " ";
            S = S.Trim();

            string[] R = S.Split(' ');
            foreach (string item in R)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    result.Add(item);
                }
            }

            return result;
        }
    }
}