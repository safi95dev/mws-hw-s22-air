﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace HIA.Models
{
    public class RemoveStopWords
    {
        PrePost StopWords;

        public RemoveStopWords()
        {

            StopWords = new PrePost(166);
            StopWords.AddString("في"); StopWords.AddString("على");
            StopWords.AddString("إلى"); StopWords.AddString("من");
            StopWords.AddString("عن"); StopWords.AddString("هناك");
            StopWords.AddString("أو"); StopWords.AddString("ثم");
            StopWords.AddString("نحن"); StopWords.AddString("أنا");
            StopWords.AddString("أنتم"); StopWords.AddString("أنت");
            StopWords.AddString("أنتما"); StopWords.AddString("إياه");
            StopWords.AddString("أنتن"); StopWords.AddString("إياها");
            StopWords.AddString("هو"); StopWords.AddString("إياهما");
            StopWords.AddString("هي"); StopWords.AddString("إياهم");
            StopWords.AddString("هما"); StopWords.AddString("إياهن");
            StopWords.AddString("هم"); StopWords.AddString("إياك");
            StopWords.AddString("هن"); StopWords.AddString("إياكما");
            StopWords.AddString("تلك"); StopWords.AddString("إياكم");
            StopWords.AddString("ذا"); StopWords.AddString("إياكن");
            StopWords.AddString("ذاك"); StopWords.AddString("إياي");
            StopWords.AddString("ذلك"); StopWords.AddString("إيانا");
            StopWords.AddString("ذان"); StopWords.AddString("أولاء");
            StopWords.AddString("ذانك"); StopWords.AddString("أولئك");
            StopWords.AddString("ذه"); StopWords.AddString("أولالك");
            StopWords.AddString("ذين"); StopWords.AddString("ذي");
            StopWords.AddString("ذينك"); StopWords.AddString("هؤلاء");
            StopWords.AddString("هذان"); StopWords.AddString("هاتان");
            StopWords.AddString("هذه"); StopWords.AddString("هانه");
            StopWords.AddString("هذي"); StopWords.AddString("هاتي");
            StopWords.AddString("هذين"); StopWords.AddString("هاتين");
            StopWords.AddString("هنا"); StopWords.AddString("هذا");
            StopWords.AddString("من"); StopWords.AddString("هنالك");
            StopWords.AddString("ما"); StopWords.AddString("التي");
            StopWords.AddString("أين"); StopWords.AddString("الذي");
            StopWords.AddString("أي"); StopWords.AddString("اللذين");
            StopWords.AddString("أيان"); StopWords.AddString("الذين");
            StopWords.AddString("حيثما"); StopWords.AddString("اللذان");
            StopWords.AddString("كيفما"); StopWords.AddString("اللاتي");
            StopWords.AddString("متى"); StopWords.AddString("اللتان");
            StopWords.AddString("هماكم"); StopWords.AddString("اللتيا");
            StopWords.AddString("كيف"); StopWords.AddString("اللتيا");
            StopWords.AddString("ماذا"); StopWords.AddString("اللواتي");
            StopWords.AddString("هلم"); StopWords.AddString("كأي");
            StopWords.AddString("قلما"); StopWords.AddString("كأين");
            StopWords.AddString("إن"); StopWords.AddString("إليك");
            StopWords.AddString("لا"); StopWords.AddString("إليكم");
            StopWords.AddString("لات"); StopWords.AddString("إليكما");
            StopWords.AddString("أن"); StopWords.AddString("إليكن");
            StopWords.AddString("كأن"); StopWords.AddString("عليك");
            StopWords.AddString("لعل"); StopWords.AddString("ها");
            StopWords.AddString("لكن"); StopWords.AddString("هاك");
            StopWords.AddString("أي"); StopWords.AddString("ليت");
            StopWords.AddString("أيا"); StopWords.AddString("أجل");
            StopWords.AddString("بل"); StopWords.AddString("إذما");
            StopWords.AddString("بلا"); StopWords.AddString("إذن");
            StopWords.AddString("حتى"); StopWords.AddString("إذ");
            StopWords.AddString("سوف"); StopWords.AddString("ألا");
            StopWords.AddString("عل"); StopWords.AddString("إلى");
            StopWords.AddString("في"); StopWords.AddString("أم");
            StopWords.AddString("كلا"); StopWords.AddString("أما");
            StopWords.AddString("هلا"); StopWords.AddString("كي");
            StopWords.AddString("وا"); StopWords.AddString("لم");
            StopWords.AddString("إذ"); StopWords.AddString("لن");
            StopWords.AddString("إلا"); StopWords.AddString("لو");
            StopWords.AddString("على"); StopWords.AddString("لولا");
            StopWords.AddString("عن"); StopWords.AddString("لوما");
            StopWords.AddString("قد"); StopWords.AddString("هل");
            StopWords.AddString("عدا"); StopWords.AddString("لما");
            StopWords.AddString("بعض"); StopWords.AddString("مذ");
            StopWords.AddString("سوى"); StopWords.AddString("منذ");
            StopWords.AddString("غير"); StopWords.AddString("حاشا");
            StopWords.AddString("كل"); StopWords.AddString("خلا");
            StopWords.AddString("ذات"); StopWords.AddString("لعمر");
            StopWords.AddString("عندما"); StopWords.AddString("مثل");
            StopWords.AddString("كلما"); StopWords.AddString("مع");
            StopWords.AddString("قبل"); StopWords.AddString("نحو");
            StopWords.AddString("خلف"); StopWords.AddString("حيث");
            StopWords.AddString("أمام"); StopWords.AddString("كلتا");
            StopWords.AddString("تحت"); StopWords.AddString("سيما");
            StopWords.AddString("يمين"); StopWords.AddString("أصلاً");
            StopWords.AddString("أصبح"); StopWords.AddString("بين");
            StopWords.AddString("كان"); StopWords.AddString("صار");
            StopWords.AddString("ليس"); StopWords.AddString("ظل");
            StopWords.AddString("انفك"); StopWords.AddString("عاد");
            StopWords.AddString("مادام"); StopWords.AddString("برح");
            StopWords.AddString("مازال"); StopWords.AddString("مافتئ");
            StopWords.AddString("فوق"); StopWords.AddString("و");

        }


        public static string[] stopWordsList = new string[]
       {
             "،","آض","آمينَ","آه","آهاً","آي","أ","أب","أجل","أجمع","أخ","أخذ","أصبح","أضحى",
                "أقبل","أقل","أكثر","ألا","أم","أما","أمامك","أمامكَ","أمسى","أمّا","أن","أنا",
                "أنت","أنتم","أنتما","أنتن","أنتِ","أنشأ","أنّى","أو","أوشك","أولئك","أولئكم",
                "أولاء","أولالك","أوّهْ","أي","أيا","أين","أينما","أيّ","أَنَّ","أََيُّ","أُفٍّ","إذ","إذا",
                "إذاً","إذما","إذن","إلى","إليكم","إليكما","إليكنّ","إليكَ","إلَيْكَ","إلّا","إمّا",
                "إن","إنّما","إي","إياك","إياكم","إياكما","إياكن","إيانا","إياه","إياها",
                "إياهم","إياهما","إياهن","إياي","إيهٍ","إِنَّ","ا","ابتدأ","اثر","اجل","احد",
                "اخرى","اخلولق","اذا","اربعة","ارتدّ","استحال","اطار","اعادة","اعلنت","اف",
                "اكثر","اكد","الألاء","الألى","الا","الاخيرة","الان","الاول","الاولى","التى","التي",
                "الثاني","الثانية","الذاتي","الذى","الذي","الذين","السابق","الف","اللائي","اللاتي",
                "اللتان","اللتيا","اللتين","اللذان","اللذين","اللواتي","الماضي","المقبل","الوقت",
                "الى","اليوم","اما","امام","امس","ان","انبرى","انقلب","انه","انها","او","اول","اي",
                "ايار","ايام","ايضا","ب","بات","باسم","بان","بخٍ","برس","بسبب","بسّ","بشكل","بضع","بطآن",
                "بعد","بعض","بك","بكم","بكما","بكن","بل","بلى","بما","بماذا","بمن","بن","بنا","به","بها",
                "بي","بيد","بين","بَسْ","بَلْهَ","بِئْسَ","تانِ","تانِك","تبدّل","تجاه","تحوّل","تلقاء","تلك","تلكم",
                "تلكما","تم","تينك","تَيْنِ","تِه","تِي","ثلاثة","ثم","ثمّ","ثمّة","ثُمَّ","جعل","جلل","جميع","جير",
                "حار","حاشا","حاليا","حاي","حتى","حرى","حسب","حم","حوالى","حول","حيث","حيثما","حين","حيَّ",
                "حَبَّذَا","حَتَّى","حَذارِ","خلا","خلال","دون","دونك","ذا","ذات","ذاك","ذانك","ذانِ","ذلك","ذلكم",
                "ذلكما","ذلكن","ذو","ذوا","ذواتا","ذواتي","ذيت","ذينك","ذَيْنِ","ذِه","ذِي","راح","رجع","رويدك",
                "ريث","رُبَّ","زيارة","سبحان","سرعان","سنة","سنوات","سوف","سوى","سَاءَ","سَاءَمَا","شبه","شخصا","شرع",
                "شَتَّانَ","صار","صباح","صفر","صهٍ","صهْ","ضد","ضمن","طاق","طالما","طفق","طَق","ظلّ","عاد","عام","عاما",
                "عامة","عدا","عدة","عدد","عدم","عسى","عشر","عشرة","علق","على","عليك","عليه","عليها","علًّ",
                "عن","عند","عندما","عوض","عين","عَدَسْ","عَمَّا","غدا","غير","ـ","ف","فان","فلان","فو","فى","في",
                "فيم","فيما","فيه","فيها","قال","قام","قبل","قد","قطّ","قلما","قوة","كأنّما","كأين","كأيّ",
                "كأيّن","كاد","كان","كانت","كذا","كذلك","كرب","كل","كلا","كلاهما","كلتا","كلم","كليكما",
                "كليهما","كلّما","كلَّا","كم","كما","كي","كيت","كيف","كيفما","كَأَنَّ","كِخ","لئن","لا","لات","لاسيما",
                "لدن","لدى","لعمر","لقاء","لك","لكم","لكما","لكن","لكنَّما","لكي","لكيلا","للامم","لم","لما",
                "لمّا","لن","لنا","له","لها","لو","لوكالة","لولا","لوما","لي","لَسْتَ","لَسْتُ","لَسْتُم","لَسْتُمَا","لَسْتُنَّ",
                "لَسْتِ","لَسْنَ","لَعَلَّ","لَكِنَّ","لَيْتَ","لَيْسَ","لَيْسَا","لَيْسَتَا","لَيْسَتْ","لَيْسُوا","لَِسْنَا","ما","ماانفك",
                "مابرح","مادام","ماذا","مازال","مافتئ","مايو","متى","مثل","مذ","مساء","مع","معاذ","مقابل",
                "مكانكم","مكانكما","مكانكنّ","مكانَك","مليار","مليون","مما","ممن","من","منذ","منها","مه",
                "مهما","مَنْ","مِن","نحن","نحو","نعم","نفس","نفسه","نهاية","نَخْ","نِعِمّا","نِعْمَ","ها","هاؤم",
                "هاكَ","هاهنا","هبّ","هذا","هذه","هكذا","هل","هلمَّ","هلّا","هم","هما","هن","هنا","هناك","هنالك",
                "هو","هي","هيا","هيت","هيّا","هَؤلاء","هَاتانِ","هَاتَيْنِ","هَاتِه","هَاتِي","هَجْ","هَذا","هَذانِ","هَذَيْنِ",
                "هَذِه","هَذِي","هَيْهَاتَ","و","و6","وا","واحد","واضاف","واضافت","واكد","وان","واهاً","واوضح",
                "وراءَك","وفي","وقال","وقالت","وقد","وقف","وكان","وكانت","ولا","ولم","ومن","وهو","وهي",
                "ويكأنّ","وَيْ","وُشْكَانَ","يكون","يمكن","يوم","أيّان" };


        public string removing(string W)
        {
            string Stem = "";
            W = W.Trim();

            string S = W.ToString();
            if (!StopWords.Search(S))
            {
                Stem += S;
            }
            return Stem;
        }

        public string StopWordsStrip( string text)
        {
            string Stem = "";
            RemoveStopWords SW = new RemoveStopWords();

                string S = ""; 

                for (int l = 0; l < text.Length; l++)
                    if (char.IsLetter(text, l))
                        S += text[l].ToString();
                    else
                        S += " ";
                S = S.Trim();

                string[] R = S.Split(' ');

                for (int j = 0; j < R.Length; j++)
                {
                    Stem += SW.removing(R[j]);
                    Stem += " ";
                }
                

            return Stem;
        }

        public string ArabicStemming(string text)
        {
            ISRI Stemmer = new ISRI();
            string stem = "";

                string S = ""; 

                for (int l = 0; l < text.Length; l++)
                    if (char.IsLetter(text, l))
                        S += text[l].ToString();
                    else
                        S += " ";

                S = S.Trim();

                string[] R = S.Split(' ');

                for (int j = 0; j < R.Length; j++)
                {
                    stem += Stemmer.Stemming(R[j]);
                    stem += " ";
                }
            
            return stem;

        }

        internal bool HasArabicCharacters(string text)
        {
            Regex regex = new Regex(
              "[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]");
            return regex.IsMatch(text);
        }


        public static char[] splitationWords = {
                        '’' ,  '،', '؟', '(', ')', '[', ']', '{', '}', '<', '>' ,  ':' , ',' ,
                        '‒', '–', '—', '―' , '…' ,  '!' ,   '.' ,  '-', '‐' ,  '?' ,  '‘', '’', '“', '”' ,  ';' ,'/' ,
                        '⁄' ,  '␠' ,  '·' , '&' ,  '@' , '*' , '\\' , '•' ,'^' ,
                        '¤', '¢', '$', '€', '£', '¥', '₩', '₪' ,  '†', '‡' , '°' ,  '¡' ,  '¿' ,
                        '¬' , '#' ,  '№' ,'%', '‰', '‱' , '¶' ,  '′' , '§' , '~' , '¨' , '_' ,  '|', '¦' ,
                        '⁂' ,  '☞' ,  '∴' , '‽' ,'※',  '\r','\n', '\t', '\v', '\f', ' ', };



    }
}